#!/bin/bash
# create file log nginx includes access by user into server web and error if  problem arise
mkdir -p /var/www/app/.docker/logs/nginx
touch /var/www/app/.docker/logs/nginx/access.log /var/www/app/.docker/logs/nginx/error.log

chmod -R 644 /var/www/app/.docker/logs/nginx/access.log
chmod -R 644 /var/www/app/.docker/logs/nginx/error.log

# Create certs
certDir=".docker/services/nginx/certs"

if [ ! -f "$certDir/ssl.key" ]; then
  mkdir -p $certDir
  openssl genrsa 2048 > "$certDir/ssl.key"
  openssl req -new -x509 -nodes -days 365 -subj "/C=VN/ST=Ha Noi/L=Ha Noi City/O=ABC/OU=ABC/CN=ABC" -key "$certDir/ssl.key" -out "$certDir/ssl.crt"
  chmod 700 "$certDir/ssl.key"
  chmod 700 "$certDir/ssl.crt"
fi
# After using openssl to generate certificate files, we need to keep the nginx container running
nginx -g "daemon off;"
