# nodejs

## Cài đặt source chỉ đơn giản chạy lệnh sau 
- docker-compose up -d

## dưới đây là quá trình xây dựng source code nodejs mọi người có thể tham khảo

## Cài đặt ExpressJS

```
npm init
npm install express
npx express-generator
```

## Cài đặt package liên quan

```
npm install --save sequelize
npm install --save mysql2
npm install --save-dev sequelize-cli
npx sequelize-cli init
npm install --save nodemon
npm install --save dotenv
```


## Sequelize

là package giúp chúng ta thao tác với cơ sở dữ liệu một cách nhanh chóng thuận tiện và dễ sử dụng, ngoài ra chúng còn có hỗ trợ kết nối với rất nhiều cơ sở dữ liệu nếu bạn thích có thể đọc qua docs nhé.

## Mysql 
là package hỗ trợ kết nối tới mysql nhé.

## Sequelize-cli 
giúp chúng ta tạo model, migrate, seed , giống như Laravel ý.

## dotenv 
Giúp chúng ta tạo ra biến môi trường và sử dụng nó ở mọi nơi. tạo file .env để sử dụng thôi nhé

## nodemon 
package này sẽ lắng nghe sự thay đổi của file và cập nhập chúng thay vì phải chạy lại ứng dụng bằng cơm .

## chay source 
docker-compose up


## Tạo model và migrate

```
docker exec -it nodejs sh
npx sequelize-cli model:generate --name Student --attributes firstName:string,lastName:string,email:string
npx sequelize-cli db:migrate
```
## Tài liệu tham khảo
https://viblo.asia/p/xay-dung-nodejs-expressjs-voi-mysql-su-dung-docker-va-docker-compose-aWj53xrbK6m